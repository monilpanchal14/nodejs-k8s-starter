# Hello NodeJS example for Kubernetes

This repository contains a *Hello NodeJS* sample application that contains the necessary configuration files to be run on a Kubernetes cluster via Minikube.

It starts a basic webserver and prints `Hello World:`

## Using Minikube?

Run Minikube locally by using the following command

```
minikube start
```

### Deploying the starter project in minikube via a single bash command

All the necessary commands for creating the deployment, service and the cronjob are added in script run.sh

```
bash run.sh
```

### Accessing the list of pods

Post successful deployment, 2 pods will get created naming hello-nodejs-ID1 and hello-nodejs-ID2

```
kubectl get pods
```

### Accessing the list of cronJob

Post successful deployment, 1 cronjob will get created naming cronjob-nodejs

```
kubectl get cronjob
```


### Accessing the application

This endpoint can then be run via curl:

```
curl $(minikube service hello-nodejs-service --url)
```

### Screenshots

Pods

![ service pods](Screenshots/Screen Shot 2018-03-09 at 9.42.49 AM.png)

CronJob

![ cronjob](Screenshots/Screen Shot 2018-03-09 at 9.43.01 AM.png)
